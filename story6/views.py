from django.shortcuts import render, redirect
from .models import Tweet
from .forms import TweetForm

# Create your views here.
def index(request):
    response = {'tweet_form':TweetForm}
    if(request.method == "POST"):
        form = TweetForm(request.POST)
        if(form.is_valid()):
            content = form.cleaned_data['tweet']
            tweet = Tweet(tweet=content)
            tweet.save()

    tweets = Tweet.objects.all()
    response['tweets'] = tweets
    return render(request, 'index.html', response)

def delete_tweet(request):
    if(request.method == "POST"):
        tweet_id = request.POST.get('id','')
        tweet = Tweet.objects.get(id=tweet_id)
        tweet.delete()

    return redirect('index')
